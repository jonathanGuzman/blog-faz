import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import ApolloClient from 'apollo-client'
import { HttpLink } from 'apollo-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { ApolloProvider } from 'react-apollo'

const client = new ApolloClient({
    link: new HttpLink({
        uri: 'http://localhost:5000/graphql',
        credentials: 'same-origin'
    }),
    cache: new InMemoryCache(),
})
const Root = () =>{
    return(<ApolloProvider client={client}><App /></ApolloProvider>);
}
ReactDOM.render(<Root/>, document.getElementById('root'));
registerServiceWorker();
