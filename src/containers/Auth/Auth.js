import React from 'react'
import gql from "graphql-tag";
import { Query } from "react-apollo";
import LinearProgress from '@material-ui/core/LinearProgress';

const GET_USER = gql`
query  {
    me{
        id,
    }
}
`;
const AuthContext = React.createContext();
export const AuthConsumer = AuthContext.Consumer;
class Auth extends React.Component {

  render() {
    return (
       <Query
        query={GET_USER} 
        >
        {({ loading, error, data }) => {
              console.log(data);
          if (error) return null //<Error />   
          if (loading || !data) return <LinearProgress />
          if (data.me === null) return (
            <AuthContext.Provider
            value={{
              isAuth: true,
              user: data.me
            }}
            >
            {this.props.children}
          </AuthContext.Provider>)
          return (  <AuthContext.Provider
            value={{
              isAuth: true,
              user: data.me
            }}
            >
            {this.props.children}
          </AuthContext.Provider>)
        }}
        </Query>)
    }
}

export default Auth;