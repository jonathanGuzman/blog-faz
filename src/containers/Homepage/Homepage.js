import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid'
import PostList from '../../components/PostList/PostList'
import Typography from '@material-ui/core/Typography';
import './Homepage.css'

const styles = theme => ({
  parentImg: {
    width: '100%',
    height: '60vh',
    overflow: 'hidden',
    position: 'relative',
    cursor: 'pointer',
  },
  defaultImg: {
    backgroundImage: "url('https://blog.jonathanfaz.com/content/images/2018/03/code-1839406_1920.jpg')",
    height: "60vh",
    backgroundSize: "cover",
    background: "no-repeat scroll center center/cover #000",
    transition: 'all .5s',
    '&:hover, &:focus': {
      transform: 'scale(1.2)'
    },
  },
  sectionPost: {
    padding: "20px 10px"
  },
  homeTitle:{
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
    fontSize: "5rem"
  }
});

class Homepage extends Component {
    render() {  
      const { classes } = this.props;
        return (
            <Grid
              container
              style={{marginTop: "64px"}}
              className="Homepage"
            >
            <Grid
              container
              className={classes.parentImg}
            >
              <Grid  
                container
                className={classes.defaultImg}
                direction="row"
                justify="center"
                alignItems="center"
              >
                <Typography
                  className={classes.homeTitle}
                  variant="display4" 
                  gutterBottom
                >
                  THE WEB DEVELOPER BLOG
                </Typography>
              </Grid>
            </Grid>
              <Grid
                container
                className={classes.sectionPost}
              >
                <PostList/>
              </Grid>
            </Grid>
        )
    }
}

export default withStyles(styles)(Homepage);
