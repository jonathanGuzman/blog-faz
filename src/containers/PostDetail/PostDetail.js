import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Post from '../../components/Post/Post';
const styles = theme => ({
  defaultImg: {
    backgroundImage: "url('https://blog.jonathanfaz.com/content/images/2018/03/code-1839406_1920.jpg')",
    height: "60vh",
    backgroundSize: "cover",
    background: "no-repeat scroll center center/cover #000"
  },
  sectionPost: {
    padding: "20px 10px"
  }
  
});

class PostDetail extends Component {
    render() {  
      const { classes } = this.props;
        return (
            <Grid
              container
              style={{marginTop: "64px"}}
            >
              <Post
                id={this.props.match.params.postId}
              />
            </Grid>
        )
    }
}

export default withStyles(styles)(PostDetail);
