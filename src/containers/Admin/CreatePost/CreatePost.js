import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import PostEditor from '../../../components/PostEditor/PostEditor';

const styles = theme => ({
  CodeContainer: {
    height: window.innerHeight-264 + 'px',
  }  
});

class CreatePost extends Component {
    constructor() {
        super();
        this.state = {
            title: '',
            value:'# Write a New Post :D'
        };
    }

    updateCode = (newCode) => {
		this.setState({
			value: newCode
        });
    }
    
    handleChange = event => {
        this.setState({ title: event.target.value });
    };
    render(){
        return (
            <div
                style={{marginTop: "64px"}}
            >
                <PostEditor 
                    value={this.state.value} 
                    titlePost={this.state.title} 
                    titleHandler={this.handleChange} 
                    changed={this.updateCode} 
                />
            </div>
        )
    }
}


export default withStyles(styles)(CreatePost);
