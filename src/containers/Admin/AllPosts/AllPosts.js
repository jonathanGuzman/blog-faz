import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import PostTable from '../../../components/PostTable/PostTable';

const styles = theme => ({
  CodeContainer: {
    height: window.innerHeight-264 + 'px',
  }  
});

class AllPosts extends Component {
    constructor() {
        super();
        this.state = {
            title: '',
            value:'# Write a New Post :D'
        };
    }

    updateCode = (newCode) => {
		this.setState({
			value: newCode
        });
    }
    
    handleChange = event => {
        this.setState({ title: event.target.value });
    };
    render(){
        return (
            <div
                style={{marginTop: "64px"}}
            >
                <PostTable/>
            </div>
        )
    }
}


export default withStyles(styles)(AllPosts);
