import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import PostEditor from '../../../components/PostEditor/PostEditor';
import gql from "graphql-tag";
import { Redirect } from 'react-router';
import { Query } from "react-apollo";
const GET_POST_BY_ID = gql`
query GetPostById($id: ID!) {
    post(id: $id){
      id,   
      title,
      description
    }
  }
`;
const styles = theme => ({
  CodeContainer: {
    height: window.innerHeight-264 + 'px',
  }  
});

class UpdatePost extends Component {
    constructor() {
        super();
        this.state = {
            title: '',
            value: ''
        };
    }

    updateCode = (newCode) => {
		this.setState({
			value: newCode
        });
    }
    
    handleChange = event => {
        this.setState({ title: event.target.value });
    };
    render(){
        console.log(this.props.postId);
        return (
            <div
                style={{marginTop: "64px"}}
            >
            <Query 
                query={GET_POST_BY_ID} 
                variables={{id:this.props.postId}}
            >                    
            {({ loading, error, data, refetch }) => {
            if (error) return null//<Error />
            if (loading || !data) return null // <Fetching />
            if (data.post === null) return <Redirect to="/admin/post"/>
            
            return (
        
                <PostEditor 
                    value={this.state.value} 
                    titlePost={this.state.title} 
                    titleHandler={this.handleChange} 
                    changed={this.updateCode} 
                />
            )

            }}
            </Query>
            </div>
        )
    }
}


export default withStyles(styles)(UpdatePost);
