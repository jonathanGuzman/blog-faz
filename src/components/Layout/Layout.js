import React, { Component, Fragment } from 'react';
import AppBar from '@material-ui/core/AppBar';
import { withStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router-dom';
import Grid from '@material-ui/core/Grid'
import Drawer from '@material-ui/core/Drawer';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import { Link } from 'react-router-dom';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import LoginDialog from './LoginDialog/LoginDialog';
import { AuthConsumer } from '../../containers/Auth/Auth';
//icons
import MenuIcon from '@material-ui/icons/Menu';
import NotesIcon from '@material-ui/icons/Notes';
import HomeIcon from '@material-ui/icons/Home';
const styles = theme => ({
  drawerGrid: {
    width: 250,
  },
  footer: {
    height: "30vh",
  },
  footerLinks: {
    margin: "10px"
  },
  alignTitle:{
    flexGrow: 1,
    textAlign: "left"
  }
});

class Layout extends Component {
    constructor() {
        super();
        this.state = {
            isOpen: false,
            openLogin: false
        };
    }
    toggleDrawer = (open) => () => {
        this.setState({
            isOpen: open
        });
    };
    handleOpen = ()=> {
        this.setState({ openLogin: true });
    }
    handleClose = () => {
        this.setState({ openLogin: false });
    };
    closeDialog = () => () => {
        this.setState({
            openLogin: false
        });
    };

    DrawerOptions = () => {
        return(     
            <div style={{width: "100%"}}>
                <List component="nav">
                    <ListItem button>
                        <ListItemIcon>
                            <NotesIcon />
                        </ListItemIcon>
                        <ListItemText primary="Notes" />
                    </ListItem >           
                </List>
            </div>
        );      
    }
    render() {  
        console.log(this.props);
        const { classes } = this.props; 
        return (    
            <Fragment>
                <AuthConsumer>
                {({isAuth})=> 
                <AppBar
                    position="fixed" >
                    <Toolbar>
                        <IconButton onClick={this.toggleDrawer(true)} color="inherit" aria-label="Menu">
                            <MenuIcon />
                        </IconButton>  
                        <Typography 
                            variant="title" 
                            color="inherit" 
                            className={classes.alignTitle}
                        >
                            Blog
                        </Typography>
                        <IconButton component={Link} to="/" replace={this.props.location.pathname === "/"} onClick={this.handlehref} color="inherit" aria-label="Menu">
                            <HomeIcon/>
                        </IconButton>
                        { isAuth ? (
                            <Button
                                href="/auth/facebook/logout"
                                color="inherit"
                            >
                            Sign out
                            </Button>
                        ) : (
                            <Button
                                onClick={this.handleOpen} 
                                color="inherit"
                            >
                            Login
                            </Button>
                        )
                        }   
                    </Toolbar>
                <Drawer open={this.state.isOpen} onClose={this.toggleDrawer(false)}>
                    <Grid
                    container
                    direction="column"
                    justify="flex-start"
                    alignItems="flex-start"
                    className={classes.drawerGrid}
                    >   
                        { this.DrawerOptions() }   
                    </Grid>
                </Drawer>
                </AppBar>
                }
                </AuthConsumer>       
                <LoginDialog
                closeDialog={this.closeDialog}
                open={this.state.openLogin}
                onClose={this.handleClose}
                />
                {    
                    this.props.children
                }
                <Grid 
                    container
                    direction="column"
                    justify="center"
                    alignItems="center"
                    className={classes.footer}
                >
                    <Grid 
                        container
                        direction="row"
                        justify="center"
                        alignItems="center"
                    >

                    <Typography className={classes.footerLinks} variant="caption" gutterBottom>
                        <a>Home</a>
                    </Typography>
                    
                    <Typography className={classes.footerLinks} variant="caption" gutterBottom>
                        <a>linkedin</a>
                    </Typography>
                    
                    <Typography className={classes.footerLinks} variant="caption" gutterBottom>
                        <a>Contact</a>
                    </Typography>
                    </Grid>
                    <Typography variant="caption" gutterBottom align="center">
                        © Copyright 2018 Jonathan Guzman
                    </Typography>
                    
                </Grid>
            </Fragment>     
        )
    }
}

export default withRouter(withStyles(styles)(Layout));

