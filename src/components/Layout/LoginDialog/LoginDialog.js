import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';

const styles = {
  itemText: {
    color: "#fff"
  }
}
const LoginDialog = (props) => {
  
    const { classes, ...other } = props;
    console.log("other",other);
    return (
      <Dialog onClose={props.handleClose} aria-labelledby="simple-dialog-title" {...other}>
        <Grid container 
          justify="space-between"
          alignItems="center"
          direction="row"
        >
        <DialogTitle id="simple-dialog-title">Sign In</DialogTitle>
        <IconButton onClick={props.closeDialog()} color="inherit" aria-label="Close">
          <CloseIcon />
        </IconButton>
        </Grid>
        <div>
          <List>
            <ListItem button component="a" href="/auth/facebook" style={{
                color: "#fff",
                backgroundColor: "#2d4373",
                borderColor: "#293e6a",              
               }}>
              <ListItemText 
                classes={{
                  primary: classes.itemText, 
                }}
                primary="Sign in with Facebook" />
            </ListItem>
          </List>
        </div>
      </Dialog>
    );
}

export default withStyles(styles)(LoginDialog);