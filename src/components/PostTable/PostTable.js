import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import gql from "graphql-tag";
import { Query } from "react-apollo";
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
const GET_POSTS = gql`
  {
    posts {
      id,   
      title,
      description
    }
  }
`;
const CustomTableCell = withStyles(theme => ({
    head: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    body: {
      fontSize: 14,
    },
  }))(TableCell);
  
  const styles = theme => ({
    root: {
      width: '100%',
      marginTop: theme.spacing.unit * 3,
      overflowX: 'auto',
    },
    table: {
      minWidth: 700,
    },
    row: {
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.background.default,
      },
    },
  });
const PostList = (props) => {
    const { classes } = props; 
  return (
    <Query 
        query={GET_POSTS} 
    >                    
        {({ loading, error, data, refetch }) => {
        if (error) return null//<Error />
        if (loading || !data) return null // <Fetching />
        return (
            <Paper className={classes.root}>
                <Table className={classes.table}>
                <TableHead>
                    <TableRow>
                        <CustomTableCell>Title</CustomTableCell>
                        <CustomTableCell>Edit</CustomTableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                {data.posts.map((post)=>{
                    return (
                    <TableRow 
                        className={classes.row} 
                        key={post.id}
                    >
                        <CustomTableCell 
                            component="th"
                            scope="row"
                        >
                        {post.title}
                        </CustomTableCell>
                        <CustomTableCell>
                            <Button 
                            component={ Link } 
                            to={"/admin/edit/post/"+post.id}
                            size="small" 
                            color="primary">
                                Edit
                            </Button>        
                        </CustomTableCell>
                    </TableRow>
                    );
                })}
                </TableBody>
            </Table>
            </Paper>
        )
        }}
    </Query>)
}
export default withStyles(styles)(PostList);