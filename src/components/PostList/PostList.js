import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import gql from "graphql-tag";
import { Query } from "react-apollo";
import PostItem from './PostItem/PostItem'
const GET_POSTS = gql`
  {
    posts {
      id,   
      title,
      description
    }
  }
`;
const styles = {
    card: {
      maxWidth: 250,
      margin: "50px auto"
    },
    media: {
      height: 140,
    },
  };

const PostList = (props) => {
  return (
    <Query 
        query={GET_POSTS} 
    >                    
        {({ loading, error, data, refetch }) => {
        if (error) return null//<Error />
        if (loading || !data) return null // <Fetching />
        return data.posts.map((post)=>
        <PostItem key={post.id}
        img="https://material-ui.com/static/images/cards/contemplative-reptile.jpg" 
        imgTitle="reptile"
        id={post.id}
        title={post.title} 
        description={post.description}
        refresh={() => refetch()}
        />)
        }}
    </Query>)
}
export default withStyles(styles)(PostList);