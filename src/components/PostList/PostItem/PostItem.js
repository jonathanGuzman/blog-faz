import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import { Link } from 'react-router-dom';
const styles = {
    card: {
      maxWidth: 250,
      margin: "50px auto"
    },
    media: {
      height: 140,
    },
  };

const PostItem = (props) => {
  const { classes } = props;
  return (
    <Grid item xs={12} 
      sm={6} 
      md={4}
      lg={3}
    >
      <Card className={classes.card} spacing={16}>
        <CardActionArea>
          <CardMedia
            className={classes.media}
            image={props.img}
            title={props.imgTitle}
          />
          <CardContent>
            <Typography gutterBottom variant="headline" component="h2">
              {
                  props.title
              }
            </Typography>
            <Typography component="p">
              {
                  props.description
              }
            </Typography>
          </CardContent>
        </CardActionArea>
        <CardActions>
          <Button size="small" color="primary">
            Share
          </Button>
          <Button component={ Link } to={"/post/"+props.id}
          size="small" color="primary">
            Learn More
          </Button>
        </CardActions>
      </Card>
    </Grid>)
}
export default withStyles(styles)(PostItem);