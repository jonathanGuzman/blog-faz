import React from 'react';
import gql from "graphql-tag";
import { Query } from "react-apollo";
import PostHeader from './PostHeader/PostHeader';
import PostBody from './PostBody/PostBody';
import LinearProgress from '@material-ui/core/LinearProgress';
import { Redirect } from 'react-router';
const GET_POSTS = gql`
query getPost($id: ID!) {
    post(id: $id) {
      id,   
      title,
      description
    }
  }
`;
const Post = ({id}) => {
  return (
    <Query
        query={GET_POSTS} 
        variables={{id}}
    >                    
        {({ loading, error, data }) => {
          if (error) return null //<Error />   
          if (loading || !data) return <LinearProgress />
          if (data.post === null) return <Redirect to="/"/>
          return (<PostBody description={data.post.description} 
          />)
        }}
    </Query>)
}
export default Post;