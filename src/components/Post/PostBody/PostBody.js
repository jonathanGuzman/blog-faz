import React from 'react';
import ReactMarkdown from 'react-markdown';
import Grid from '@material-ui/core/Grid';

const PostBody = (props) => {
  return (
    <Grid item 
    xs={12} 
    >
      <ReactMarkdown source={ props.description } mode="raw"/>
    </Grid>)
}
export default PostBody;