import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import gql from "graphql-tag";
import Button from '@material-ui/core/Button';
import { Mutation } from 'react-apollo';
const SAVE_POST = gql`
  mutation savePost($title: String, $description: String) {
    createPost(title: $title, description: $description) {
        id,
        title,
        description
    }
  }
`;
const styles = theme => ({
    button: {
      margin: theme.spacing.unit,
    },
    input: {
      display: 'none',
    },
  });
const PostEditor = (props) => {
    const { classes } = props;
    const { title, description } = props;
  return (
    <Grid  
        container
        direction="row"
    > 
    <Mutation mutation={SAVE_POST} >
    {(savePost, { data, loading, error })  => (
        <Button 
            variant="contained"
            color="primary"
            className={classes.button} 
            onClick={async () => {
                await savePost({ variables: { title, description } })
                this.props.history.push('/');
            }}
        >
        Save
      </Button>
    )}
  </Mutation>  
        <input
            accept="image/*"
            className={classes.input}
            id="contained-button-file"
            multiple
            type="file"
        />
        <label htmlFor="contained-button-file">
            <Button 
                variant="contained" 
                component="span"
                className={classes.button} 
            >
            Upload
            </Button>
        </label>
    </Grid>
  )
}


export default withRouter(withStyles(styles)(PostEditor));
