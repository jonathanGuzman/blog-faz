import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import CodeMirror from 'react-codemirror';
import './PostEditor.css'
import 'codemirror/lib/codemirror.css';
import ReactMarkdown from 'react-markdown';
import Controls from './Controls/Controls';

import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
const styles = theme => ({
    textField: {
      marginLeft: theme.spacing.unit,
      marginRight: theme.spacing.unit,
    },
})
const PostEditor = (props) => {
    const options = {
			lineNumbers: true,
			readOnly: false,
			mode: 'markdown',
            theme: "monokai"
        };
    const { classes } = props;
  return (
    <Grid  
        container
        direction="row"
    >   
        <Controls 
            title={"Title WEB post"}
            description={props.value}
        />
        
        <TextField
          required
          id="standard-required"
          label="Title"
          value={props.titlePost}
          margin="normal"
          fullWidth
          style={{ margin: 8 }}
          onChange={props.titleHandler}
        />
        <Grid item xs={12} sm={6}>
            <CodeMirror value={props.value} onChange={props.changed} options={options} />
        </Grid>
        <Grid item xs={12} sm={6}>
            <ReactMarkdown source={props.value}/>
        </Grid>
    </Grid>
  )
}


export default withStyles(styles)(PostEditor);