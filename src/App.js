import React, { Component } from 'react';
import './App.css';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import { MuiThemeProvider,  createMuiTheme } from '@material-ui/core/styles';
import blue from '@material-ui/core/colors/blue';
import Layout from './components/Layout/Layout';
import Homepage from './containers/Homepage/Homepage';
import PostDetail from './containers/PostDetail/PostDetail';
import CreatePost from './containers/Admin/CreatePost/CreatePost';
import UpdatePost from './containers/Admin/UpdatePost/UpdatePost';
import AllPosts from './containers/Admin/AllPosts/AllPosts';
import Auth, { AuthConsumer } from './containers/Auth/Auth'
const theme = createMuiTheme({
  palette: {
    primary: blue,
  },
});
class App extends Component {
  render() {
    return (
    <BrowserRouter>
      <MuiThemeProvider theme={theme}>
        <Auth>
          <AuthConsumer>
            {({isAuth})=>(
              <Layout>
                <Switch>
                    <Route exact path="/" component={Homepage}/>
                    <Route path="/post/:postId" component={PostDetail}/>
                    <Route path="/admin/create/post" component={(props)=>(
                      isAuth ?
                      <CreatePost/>
                      : 
                      <Redirect to="/"/>
                    )
                    }/>
                    <Route path="/admin/post" component={(props)=>(
                      isAuth ?
                      <AllPosts/>
                      : 
                      <Redirect to="/"/>
                    )
                    }/>
                    <Route path="/admin/edit/post/:postId" component={(props)=>{console.log(props);return(
                      isAuth ?
                      <UpdatePost 
                        postId={ props.match.params.postId }
                      />
                      : 
                      <Redirect to="/"/>
                    )}
                    }/>
                </Switch>
              </Layout>
            )}
          </AuthConsumer>
        </Auth>
      </MuiThemeProvider>
    </BrowserRouter>
    );
  }
}

export default App;
